﻿using System;
using System.Collections.Generic;

// Define the Fruit hierarchy
abstract class Fruit
{
    public abstract void Accept(IFruitVisitor visitor);
}

class Apple : Fruit
{
    public override void Accept(IFruitVisitor visitor)
    {
        visitor.VisitApple(this);
    }
}

class Banana : Fruit
{
    public override void Accept(IFruitVisitor visitor)
    {
        visitor.VisitBanana(this);
    }
}

// Define the visitor interface
interface IFruitVisitor
{
    void VisitApple(Apple apple);
    void VisitBanana(Banana banana);
}

// Implement concrete visitors
class ShoppingCartVisitor : IFruitVisitor
{
    public void VisitApple(Apple apple)
    {
        Console.WriteLine("Adding an apple to the shopping cart.");
    }

    public void VisitBanana(Banana banana)
    {
        Console.WriteLine("Adding a banana to the shopping cart.");
    }
}

class NutritionVisitor : IFruitVisitor
{
    public void VisitApple(Apple apple)
    {
        Console.WriteLine("Analyzing the nutritional value of an apple.");
    }

    public void VisitBanana(Banana banana)
    {
        Console.WriteLine("Analyzing the nutritional value of a banana.");
    }
}

class Program
{
    static void Main(string[] args)
    {
        var fruits = new List<Fruit>
        {
            new Apple(),
            new Banana()
        };

        var shoppingCartVisitor = new ShoppingCartVisitor();
        var nutritionVisitor = new NutritionVisitor();

        foreach (var fruit in fruits)
        {
            fruit.Accept(shoppingCartVisitor);
            fruit.Accept(nutritionVisitor);
        }
    }
}
